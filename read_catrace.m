function [trace, full_path]=read_catrace(dateiname)
if nargin < 1
    info = 'Please select .mat file';
    [fname,pathname]=uigetfile('*.mat',info);
    full_path=[pathname fname];
else
    full_path = dateiname;
end


temp = load(full_path);
display('mat file loaded');
temp2=struct2cell(temp);
temp2 = temp2';

%%%%%%uncomment if error in matlab file
%tmp = size(temp2); tmp = tmp(2)-1
%temp2 = temp2'
%temp2 = temp2{1, 1:tmp}
%%%%%%
trace=cell2mat(temp2);

end