
[mat, filepath] = read_catrace();
[filepath,name,ext] = fileparts(filepath);
endout=regexp(filepath,filesep,'split');
trace = mat(1,:); % 1 if year >= 2018, else 2
%precision necessary for 1/2000 = 0.0005
dlmwrite([endout{1,6} '.dat'], trace, 'delimiter','\n', 'precision','%.4f');
plot(trace)