# Calcium data from the GAERS model

This repo contains data from calcium recordings with seizures from the GAERS model.

## Structure

  * MatlabData contains the raw data with marked seizures by a trained electrophysician in the Matlab file format.
  * RawData is the calcium data in text format from the matlab files.
  * ValidatorData has files which contains the start and end points of the seizures in text format.

The data points in ValidatorData are moved to be at the maximum of a spike-wave discharge.
